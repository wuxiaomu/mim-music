# Mim Music

#### 介绍
Mim Music , play modern music。

#### 关于
Mim Music , 是一款简单的音乐播放器，使用BASS类进行音乐播放，彗星的桌面歌词显示，雾削木的托盘模块。
目前正在开发，只能听取从网易云获取的相关音乐，后续会更新更多音乐源。

![test1](/Test01.png)
![test2](/Test02.png)

#### 安装教程
您只需下载Mim主程序即可直接运行。

#### 使用说明
启动后会在目录中生成文件，system.ini、list.json等；
1. system.ini
system.ini为相关配置项，后续会持续增加，删除后启动设置将会回归初始状态。

2. list.json
list.json为本地歌单列表，使用json标准格式存储，后续会持续更新。

3. config.ini
config.ini为初始小配置。


#### 相关链接

1.    https://space.bilibili.com/323611141
2.    https://www.bilibili.com/video/BV1vV411n7Le
3.    https://www.bilibili.com/video/BV1qB4y1z71o
4.    https://www.bilibili.com/video/BV1qb4y1e76D
5.    https://www.bilibili.com/video/BV1BY411w7mz